# SavePlayListYouTube

description : 
cet outil permet de télécharger des playlist youtube en qualité HD depuis une liste de chaine youtube renseigné dans un fichier texte


pré requis : 
- avoir installer youtube-dl sur son poste client 
- etre sous linux ou unix avec bash

execution : 

`./download.sh `

le fichier ./playlist.txt est formé de la façon suivante : 

```
# commentaire
titre;URL de la chaine 
```


les lignes vides et les commentaires ne sont pas pris en compte. 
seules les lignes correcte sont prise en comptes.
